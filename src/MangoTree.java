import java.util.Scanner;

public class MangoTree {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int numberOfRows=sc.nextInt();
        int numberOfColumns=sc.nextInt();
        int treeNumber=sc.nextInt();
        String mangoTree=isMango(numberOfColumns,treeNumber) ;
        System.out.println(mangoTree );

    }

    static String isMango(int numberOfColumns, int treeNumber){
        if (treeNumber<=numberOfColumns){
            return "yes";
        }
        else if(treeNumber % numberOfColumns==0 || treeNumber % numberOfColumns==1){
            return "yes";
        }
        else{
            return "no";
        }
    }
}

