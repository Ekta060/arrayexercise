import java.util.Scanner;

public class MagicalSquare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] square = new int[n][n];
        for (int i = 0; i < square.length; i++) {
            for(int j=0;j<square[i].length;j++) {
                square[i][j] = sc.nextInt();
            }
        }
        boolean magicSquare=isMagicSquare(square);
        if(magicSquare){
            System.out.println("yes");
        }
        else{
            System.out.println("no");
        }
    }

    static boolean isMagicSquare(int[][] square){
        int sumForwardDiagonal=0;
        int sumBackwardDiagonal=0;
        for (int row = 0; row < square.length; row++) {
            for(int col=0;col<square[row].length;col++) {
                if (row==col){
                    sumForwardDiagonal+=square[row][col];
                }
                if((row+col)==(square.length-1)){
                    sumBackwardDiagonal +=square[row][col];
                }
            }
        }

        // if both the diagonals are not equal return false
        if (sumForwardDiagonal!=sumBackwardDiagonal ){
            return false;
        }
        else{
            for(int row=0;row< square.length;row++){
                int sumOfRow =0;
                int sumOfCol=0;
                for(int col=0; col<square[row].length;col++){
                    sumOfRow+=square[row][col];
                    sumOfCol+=square[col][row];
                }
                // if sum of row and sum of column is not equal to diagonal return false
                if(sumOfRow!=sumForwardDiagonal || sumOfCol!=sumForwardDiagonal ){
                    return false;
                }
            }
        }
        return true;
    }
}

