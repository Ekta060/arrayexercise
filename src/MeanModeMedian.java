import java.util.Arrays;
import java.util.Scanner;

public class MeanModeMedian{
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int totalElements=sc.nextInt() ;
        int[] array=new int[totalElements ];
        for(int i=0;i<totalElements ;i++){
            int element= sc.nextInt();
            array[i]=element;
        }

        double[] arrayOfMeanModeMedian= getMeanModeMedian(array) ;
        for(int i=0;i<3;i++){
            System.out.println(String.format("%.2f",arrayOfMeanModeMedian[i]));
        }
    }
    static double [] getMeanModeMedian(int[] array){
        //mode

        Arrays.sort(array);
        int sumOfArray=0;
        for(int i=0;i< array.length ;i++){
            sumOfArray  +=array[i];
        }
        double  mean= (double)  sumOfArray / (double)  array.length;

        //median

        double median=0;
        if(array.length%2==1){
            int getMedian= (array.length+1)/2;
            median=array[getMedian-1];
        }
        else{
            int a1=array[((array.length -1)/2)-1];
            int a2=array[((array.length+1)/2)-1];
            median= (double)(a1+a2)/2;
        }

        //mode
        double mode=0;
        int maxCount=0;

        for(int i=0;i<array.length;i++){
            int count=0;
            for(int j=0;j<array.length;j++){
                if(array[i]==array[j]){
                    count++;
                }
            }
            if(count>maxCount){
                maxCount=count;
                mode=array[i];
            }
        }

        double[] meanModeMedian=new double[3];
        meanModeMedian [0]=mean;
        meanModeMedian [1]=median;
        meanModeMedian [2]=mode;
        return meanModeMedian ;

    }
}

