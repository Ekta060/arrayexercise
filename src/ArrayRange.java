import java.util.Scanner;

public class ArrayRange {
    public static void main(String[] args){
        Scanner sc= new Scanner(System.in);
        int numberOfElements=sc.nextInt() ;
        int[] array=new int[numberOfElements ];

        for(int i=0;i<array.length;i++){
            int elements=sc.nextInt() ;
            array[i]=elements;
        }

        int rangeOfArray=getRange(array);
        System.out.println(rangeOfArray );
    }


    //function to get Minimum Number from Array
    static int getMin(int[] array){
        int minElement=array[0];
        for(int i=1;i<array.length;i++){
            if(minElement>array[i]){
                minElement = array[i];
            }
        }
        return minElement ;
    }


    //function to get Maximum Number from Array
    static int getMax(int[] array){
        int maxElement=array[0];
        for(int i=1;i<array.length;i++){
            if(maxElement<array[i]){
                maxElement = array[i];
            }
        }
        return maxElement  ;
    }

    // function to find range
    static int getRange(int[] array){
        int minElement=getMin(array) ;
        int maxElement=getMax(array);
        int arrayRange=maxElement - minElement ;
        return arrayRange ;
    }

}

