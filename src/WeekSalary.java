import java.util.Scanner;

public class WeekSalary {

    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int[] perDayWorkHour=new int[7];
        for(int i=0;i<7;i++){
            int elements=sc.nextInt() ;
            perDayWorkHour[i]=elements ;
        }
        int finalSalary=getSalary(perDayWorkHour);
        System.out.println(finalSalary );
    }

    //function to get salary
    static int getSalary(int[] array){
        int salary=0;
        int hour=0; //work hour per day
        int totalWorkingHour=0; //total working hour in week
        int sundayBonus=0; //Sunday Bonus
        int saturdayBonus =0; //Saturday Bonus
        for(int i=0;i<array.length;i++){
            hour=array[i];
            totalWorkingHour +=array[i];
            // work hour less than or equal to 8Hr
            if(8>=hour){
                if (i==6 && array[6]!=0){
                    saturdayBonus+=((hour*100)*25)/100;
                    salary=salary+(hour*100)+saturdayBonus ;

                }
                // bonus of saturday
                else if (i==0 && array[0]!=0){
                    sundayBonus=((hour*100)*50)/100;
                    salary=salary+(hour*100)+sundayBonus ;
                }
                // bonus of sunday
                else{
                    salary+=hour*100;
                }

            }
            // work hour more than 8 hrs in a day
            else{
                if (i==6 && array[6]!=0){
                    saturdayBonus =((8*100+((hour-8)*(115)))*25)/100;
                    salary=salary+(8*100+((hour-8)*(115)))+saturdayBonus ;

                }
                else if (i==0 && array[0]!=0){
                    sundayBonus =((8*100+((hour-8)*(115)))*50)/100;
                    salary=salary+(8*100+((hour-8)*(115)))+sundayBonus ;
                }
                else{
                    salary+=8*100+((hour-8)*(115));
                }

            }

        }
        // Total work Hour more than 40 hrs in a week.
        if(totalWorkingHour >40){
            salary=salary+((hour-40)*25)-saturdayBonus-sundayBonus  ;
        }
        return salary;
    }
}

